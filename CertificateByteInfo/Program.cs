﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace CertificateByteInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            // The path to the certificate.
            Console.WriteLine("Enter the path to the file to be converted: ");
            string Certificate = Console.ReadLine();

            try
            {
                // Load the certificate into an X509Certificate object.
                X509Certificate cert = X509Certificate.CreateFromCertFile(Certificate);

                // Get the value.
                byte[] results = cert.GetRawCertData();
                string AsBase64String = Convert.ToBase64String(results);

                // Display the value to the console.
                foreach (byte b in results)
                {
                    b.ToString();
                    Console.Write(b+",");                
                }
                Console.ReadLine();
            }
            catch
            {
                Console.Write("There was a problem reading the file");
                Console.ReadLine();
            }
        }
    }
}
